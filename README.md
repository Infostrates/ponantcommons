Infostrates' Ponant Webservices Commons
=======================================

Helpers collection used both by [Ponant Webservice](composer config repo.ponantcommons vcs git@bitbucket.org:Infostrates/ponantcommons.git) and [Ponant Onboard](https://bitbucket.org/Infostrates/ponantonboard/src/master/).

Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Step 1: Download the Bundle
---------------------------

Open a command console, enter your project directory and execute the
following commands to download the latest stable version of this bundle:

```console
$ composer config repo.ponantcommons vcs git@bitbucket.org:Infostrates/ponantcommons.git
$ composer require infostrates/ponant-webservice-commons
```

Step 2: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Infostrates\PonantWebservicesCommons\InfostratesPonantWebservicesCommonsBundle(),
        ];

        // ...
    }

    // ...
}
```
