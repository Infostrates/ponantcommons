<?php


namespace Tests\Infostrates\PonantWebservicesCommons\Traits;


trait GetResourcesDir
{
    /**
     * @return string
     */
    private function getResourcesDir(): string
    {
        return __DIR__ . '/../Resources';
    }
}