<?php

declare(strict_types=1);

namespace Tests\Infostrates\PonantWebservicesCommons\Traits;

trait GetLastElementOf
{
    private function getLastElementOf(array $elementList)
    {
        return empty($elementList) ? null : end($elementList);
    }
}
