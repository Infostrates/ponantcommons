<?php

namespace Tests\Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions\InvalidFile;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\FrenchEnglishTextValue;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\LineParseError;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\InputLine;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Parser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetResourcesDir;

class ParserTest extends TestCase
{
    use GetResourcesDir;

    /**
     * @param SplFileInfo $fileInfo
     * @dataProvider allFilesProvider
     * @throws InvalidFile
     */
    public function testAllFiles(SplFileInfo $fileInfo): void
    {
        $parser = new Parser();
        $inputLineList = $parser->parseFile($fileInfo->getPathname());
        $this->assertContainsOnlyInstancesOf(InputLine::class, $inputLineList);
    }

    public function testParseFile(): void
    {
        $parser = new Parser();
        $inputLineList = $parser->parseFile($this->getResourcesDir() . '/SpaMenu/Carte du spa DEF 20200610.xlsx');
        $this->assertCount(82, $inputLineList);
        $this->assertContainsOnlyInstancesOf(InputLine::class, $inputLineList);

        $firstInputLine = $inputLineList[0] ?? null;
        if ($firstInputLine) {
            $this->assertEquals(
                new InputLine(
                    'SOTHYS',
                    new FrenchEnglishTextValue('SOINS VISAGE ET CORPS', 'FACE & BODY TREATMENT'),
                    new FrenchEnglishTextValue('PACKS Échappée belle', 'PACKS A beauty break'),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('ESCALE EXCLUSIVE', 'EXCLUSIVE CALL'),
                    new FrenchEnglishTextValue('• Gommage délicieux | 30’
• Modelage 100% sur-mesure | 1h15’
• Soin visage énergisant | 1h30’', '• Delicious Scrub | 30’
• 100% tailored modelling | 1h15’
• Energizing facial treatment | 1h30’'),
                    '3h15',
                    'mixte',
                    240
                ),
                $firstInputLine
            );
        }

        $lastInputLine = end($inputLineList);
        if ($lastInputLine) {
            $this->assertEquals(
                new InputLine(
                    '',
                    new FrenchEnglishTextValue('ESTHETIQUE', 'BEAUTY'),
                    new FrenchEnglishTextValue('ONGLERIE', 'MANICURE'),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('Soin des pieds', 'Pedicure'),
                    new FrenchEnglishTextValue('Offrant à la fois un gommage, un ponçage et un soin hydratant, ce soin ciblé combine habillement beauté et relaxation et redonne à vos pieds douceur et souplesse.', 'Exfoliating, pumicing and moisturising, this treatment is a wonderful combination of beauty and relaxation to leave your feet soft and smooth.'),
                    '1h',
                    '',
                    70
                ),
                $lastInputLine
            );
        }

        $lineParseErrorList = $parser->getLastLineParseError();
        $this->assertCount(1, $lineParseErrorList);
        $this->assertEquals(new LineParseError(27, 'Columns B and C are mandatory'), $lineParseErrorList[0]);
    }

    public function testParseInvalidFile(): void
    {
        $this->expectExceptionObject(new InvalidFile('The row of column titles (A2:O2) does not correspond to the expected titles.'));
        $parser = new Parser();
        $parser->parseFile($this->getResourcesDir() . '/WineMenu/Carte des vins DEF 20200610.xlsx');
    }

    public static function allFilesProvider(): array
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/../../../Resources/SpaMenu');

        $dataList = [];
        foreach ($finder as $file) {
            $dataList[] = [$file];
        }

        return $dataList;
    }
}
