<?php

namespace Tests\Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Loader;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\Care;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\Menu;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\Type2;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetLastElementOf;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetResourcesDir;
use Tests\Infostrates\PonantWebservicesCommons\Web\WebTestCase;

class LoaderTest extends WebTestCase
{
    use GetLastElementOf;
    use GetResourcesDir;

    public function testLoad()
    {
        $loader = self::bootKernel()->getContainer()->get(Loader::class);
        $menuList = $loader->load($this->getResourcesDir().'/SpaMenu/Carte du spa DEF 20200610.xlsx', 'fr', 20);
        $this->assertContainsOnly(Menu::class, $menuList);
        $this->assertCount(4, $menuList);

        /** @var Menu $firstMenu */
        $firstMenu = $menuList[0] ?? null;
        if ($firstMenu) {
            $this->assertEquals('SOINS VISAGE ET CORPS', $firstMenu->getLabel());
            $this->assertCount(5, $firstMenu->getType2List());

            $firstType2 = $firstMenu->getType2List()[0] ?? null;
            if ($firstType2) {
                $this->assertEquals('PACKS Échappée belle', $firstType2->getLabel());
                $this->assertCount(3, $firstType2->getCareList());

                $lastCare = $this->getLastElementOf($firstType2->getCareList());
                if ($lastCare)
                {
                    $this->assertEquals(
                        new Care(
                            'SOIN SIGNATURE SOTHYS™ : RITUEL SECRET VISAGE ET CORPS',
                            '• Un soin corps en 3 temps : après un prélude d’éveil, un granité de gommage sublime votre corps puis un modelage nourrissant vous transporte dans une relaxation profonde.
• Un soin visage en 8 étapes : associant démaquillage sensation, gommage ultra efficace, modelages d’exception, masques haute performance, lissage final effet glaçon et remise en beauté.',
                            '2h30',
                            'mixed',
                            310.0,
                            372.0
                        ),
                        $lastCare
                    );
                }
            }
        }

        /** @var Menu $nullPriceMenu */
        $nullPriceMenu = $menuList[4] ?? null;
        if ($nullPriceMenu) {
            $this->assertEquals('ESTHETIQUE', $nullPriceMenu->getLabel());

            $nullPriceType2 = $nullPriceMenu->getType2List()[0] ?? null;
            if ($nullPriceType2) {
                $this->assertEquals('MAQUILLAGE', $nullPriceType2->getLabel());

                $nullPriceCare = $this->getLastElementOf($nullPriceType2->getCareList());
                if ($nullPriceCare)
                {
                    $this->assertEquals(
                        new Care(
                            'Test',
                            'Maquillage jour - Maquillage soir - Maquillage grand soir - Maquillage flash. À partir de 20€',
                            'Selon la prestation
Depending of the treatment.',
                            null,
                            null,
                            null
                        ),
                        $nullPriceCare
                    );
                }
            }
        }

        $menuList = $loader->load($this->getResourcesDir().'/SpaMenu/Carte du spa DEF 20200610.xlsx', 'en', 20);
        $this->assertContainsOnly(Menu::class, $menuList);
        $this->assertCount(4, $menuList);

        /** @var Menu $lastMenu */
        $thirdMenu = $menuList[2];
        $this->assertEquals('HAIR', $thirdMenu->getLabel());
        $this->assertCount(9, $thirdMenu->getType2List());

        /** @var Type2 $firstType2 */
        $firstType2 = $thirdMenu->getType2List()[0];
        $this->assertEquals(null, $firstType2->getLabel());
        $this->assertCount(1, $firstType2->getCareList());
        $lastCare = $this->getLastElementOf($firstType2->getCareList());
        $this->assertEquals(
            new Care(
                'Supplement very long hair',
                null,
                null,
                'women',
                15.0,
                18.0
            ),
            $lastCare
        );

        /** @var Type2 $lastType2 */
        $lastType2 = $this->getLastElementOf($thirdMenu->getType2List());
        $this->assertEquals('MEN', $lastType2->getLabel());
        $this->assertCount(2, $lastType2->getCareList());
        $lastCare = $this->getLastElementOf($lastType2->getCareList());
        $this->assertEquals(
            new Care(
                'Hair clipper',
                null,
                null,
                'men',
                20.0,
                24.0
            ),
            $lastCare
        );

        /** @var Menu $lastMenu */
        $lastMenu = $this->getLastElementOf($menuList);
        if ($lastMenu) {
            $this->assertEquals('BEAUTY', $lastMenu->getLabel());
            $this->assertCount(3, $lastMenu->getType2List());

            /** @var Type2 $lastType2 */
            $lastType2 = $this->getLastElementOf($lastMenu->getType2List());
            if ($lastType2) {
                $this->assertEquals('MANICURE', $lastType2->getLabel());
                $this->assertCount(8, $lastType2->getCareList());

                $lastCare = $this->getLastElementOf($lastType2->getCareList());
                if ($lastCare)
                {
                    $this->assertEquals(
                        new Care(
                            'Pedicure',
                            'Exfoliating, pumicing and moisturising, this treatment is a wonderful combination of beauty and relaxation to leave your feet soft and smooth.',
                            '1h',
                            null,
                            70.0,
                            84.0
                        ),
                        $lastCare
                    );
                }
            }
        }
    }
}
