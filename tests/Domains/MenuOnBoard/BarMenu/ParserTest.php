<?php

namespace Tests\Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions\InvalidFile;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\FrenchEnglishTextValue;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\LineParseError;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\InputLine;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Parser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetResourcesDir;

class ParserTest extends TestCase
{
    use GetResourcesDir;

    /**
     * @param SplFileInfo $fileInfo
     * @dataProvider allFilesProvider
     * @throws InvalidFile
     */
    public function testAllFiles(SplFileInfo $fileInfo): void
    {
        $parser = new Parser();
        $inputLineList = $parser->parseFile($fileInfo->getPathname());
        $this->assertContainsOnlyInstancesOf(InputLine::class, $inputLineList);
    }

    /**
     * @throws InvalidFile
     */
    public function testParseFile(): void
    {
        $parser = new Parser();
        $inputLineList = $parser->parseFile($this->getResourcesDir() . '/BarMenu/Carte des bars DEF 20200610.xlsx');
        $this->assertCount(161, $inputLineList);
        $this->assertContainsOnlyInstancesOf(InputLine::class, $inputLineList);

        $firstInputLine = $inputLineList[0] ?? null;
        if ($firstInputLine) {
            $this->assertEquals(
                new InputLine(
                    new FrenchEnglishTextValue('Cocktails', 'Cocktails'),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('Cocktail du jour', 'Daily cocktail'),
                    new FrenchEnglishTextValue('Veuillez-vous renseigner auprès de l’équipe du Bar.', 'Please ask to the Bar team.'),
                    0.0,
                    false
                ),
                $firstInputLine
            );
        }

        $lastInputLine = end($inputLineList);
        if ($lastInputLine) {
            $this->assertEquals(
                new InputLine(
                    new FrenchEnglishTextValue('Caviar', 'Caviar'),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('Caviar (15g) + flûte de Champagne Veuve Clicquot 12 cl', 'Caviar (15g) + glass of Champagne Veuve Clicquot 12 cl'),
                    new FrenchEnglishTextValue('', ''),
                    40.0,
                    false
                ),
                $lastInputLine
            );
        }

        $lineParseErrorList = $parser->getLastLineParseError();
        $this->assertCount(7, $lineParseErrorList);
        $this->assertEquals(new LineParseError(90, 'Columns G and H are mandatory'), $lineParseErrorList[0]);
        $this->assertEquals(new LineParseError(96, 'Columns G and H are mandatory'), $lineParseErrorList[6]);
    }

    public function testParseInvalidFile(): void
    {
        $this->expectExceptionObject(new InvalidFile('The row of column titles (A3:M3) does not correspond to the expected titles.'));
        $parser = new Parser();
        $parser->parseFile($this->getResourcesDir() . '/WineMenu/Carte des vins DEF 20200610.xlsx');
    }

    static public function allFilesProvider(): array
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/../../../Resources/BarMenu');

        $dataList = [];
        foreach ($finder as $file) {
            $dataList[] = [$file];
        }

        return $dataList;
    }
}
