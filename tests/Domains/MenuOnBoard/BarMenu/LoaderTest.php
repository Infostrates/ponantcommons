<?php

namespace Tests\Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Loader;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\Drink;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\Type1;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetLastElementOf;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetResourcesDir;
use Tests\Infostrates\PonantWebservicesCommons\Web\WebTestCase;

class LoaderTest extends WebTestCase
{
    use GetLastElementOf;
    use GetResourcesDir;

    public function testLoad()
    {
        $loader = self::bootKernel()->getContainer()->get(Loader::class);
        $type1List = $loader->load($this->getResourcesDir().'/BarMenu/Carte des bars DEF 20200610.xlsx', 'fr', 20);
        $this->assertContainsOnly(Type1::class, $type1List);
        $this->assertCount(8, $type1List);

        /** @var Type1 $firstType1 */
        $firstType1 = $type1List[0] ?? null;
        if ($firstType1) {
            $this->assertEquals('Cocktails', $firstType1->getLabel());
            $this->assertCount(7, $firstType1->getType2List());

            $secondType2 = $firstType1->getType2List()[1] ?? null;
            if ($secondType2) {
                $this->assertEquals('A base de champagne', $secondType2->getLabel());
                $this->assertCount(1, $secondType2->getType3List());

                $lastType3 = $this->getLastElementOf($secondType2->getType3List());
                if ($lastType3) {
                    $this->assertEquals(null, $lastType3->getLabel());
                    $this->assertCount(7, $lastType3->getDrinkList());

                    $lastDrink = $this->getLastElementOf($lastType3->getDrinkList());
                    if ($lastDrink)
                    {
                        $this->assertEquals(
                            new Drink(
                                'Ginger Fever',
                                'Champagne Veuve Clicquot, vodka Belvedere, Galliano, gingembre frais, sirop de sucre de canne',
                                9,
                                10.8,
                                true
                            ),
                            $lastDrink
                        );
                    }
                }
            }
        }

        $type1List = $loader->load($this->getResourcesDir().'/BarMenu/Carte des bars DEF 20200610.xlsx', 'en', 20);
        $this->assertContainsOnly(Type1::class, $type1List);
        $this->assertCount(8, $type1List);

        /** @var Type1 $lastType1 */
        $lastType1 = $this->getLastElementOf($type1List);
        if ($lastType1) {
            $this->assertEquals('Caviar', $lastType1->getLabel());
            $this->assertCount(1, $lastType1->getType2List());

            $lastType2 = $this->getLastElementOf($lastType1->getType2List());
            if ($lastType2) {
                $this->assertEquals(null, $lastType2->getLabel());
                $this->assertCount(1, $lastType2->getType3List());

                $lastType3 = $this->getLastElementOf($lastType2->getType3List());
                if ($lastType3) {
                    $this->assertEquals(null, $lastType3->getLabel());
                    $this->assertCount(3, $lastType3->getDrinkList());

                    $lastDrink = $this->getLastElementOf($lastType3->getDrinkList());
                    if ($lastDrink)
                    {
                        $this->assertEquals(
                            new Drink(
                                'Caviar (15g) + glass of Champagne Veuve Clicquot 12 cl',
                                null,
                                40,
                                48,
                                false
                            ),
                            $lastDrink
                        );
                    }
                }
            }
        }
    }
}
