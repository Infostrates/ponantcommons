<?php

namespace Tests\Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Loader;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\Color;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\Wine;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetLastElementOf;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetResourcesDir;
use Tests\Infostrates\PonantWebservicesCommons\Web\WebTestCase;

class LoaderTest extends WebTestCase
{
    use GetLastElementOf;
    use GetResourcesDir;

    public function testLoad()
    {
        $loader = self::bootKernel()->getContainer()->get(Loader::class);
        $colorList = $loader->load($this->getResourcesDir().'/WineMenu/Carte des vins DEF 20200610.xlsx', 'fr', 20);
        $this->assertContainsOnly(Color::class, $colorList);
        $this->assertCount(3, $colorList);

        /** @var Color $firstColor */
        $firstColor = $colorList[0] ?? null;
        if ($firstColor) {
            $this->assertEquals('VINS ROUGES', $firstColor->getLabel());
            $this->assertCount(8, $firstColor->getRegionList());

            $firstRegion = $firstColor->getRegionList()[0] ?? null;
            if ($firstRegion) {
                $this->assertEquals(null, $firstRegion->getLabel());
                $this->assertCount(1, $firstRegion->getSubRegionList());

                $lastSubRegion = $this->getLastElementOf($firstRegion->getSubRegionList());
                if ($lastSubRegion) {
                    $this->assertEquals(null, $lastSubRegion->getLabel());
                    $this->assertCount(1, $lastSubRegion->getWineList());

                    $firstWine = $lastSubRegion->getWineList()[0];
                    if ($firstWine)
                    {
                        $this->assertEquals(
                            new Wine(
                                'Vin rouge de croisière',
                                null,
                                null,
                                null,
                                null,
                                null
                            ),
                            $firstWine
                        );
                    }
                }
            }
        }

        $colorList = $loader->load($this->getResourcesDir().'/WineMenu/Carte des vins DEF 20200610.xlsx', 'en', 20);
        $this->assertContainsOnly(Color::class, $colorList);
        $this->assertCount(4, $colorList);

        /** @var Color $thirdColor */
        $thirdColor = $colorList[2];
        if ($thirdColor) {
            $this->assertEquals('ROSE WINE', $thirdColor->getLabel());
            $this->assertCount(1, $thirdColor->getRegionList());

            $lastRegion = $this->getLastElementOf($thirdColor->getRegionList());
            if ($lastRegion) {
                $this->assertEquals('PROVENCE', $lastRegion->getLabel());
                $this->assertCount(1, $lastRegion->getSubRegionList());

                $lastSubRegion = $this->getLastElementOf($lastRegion->getSubRegionList());
                if ($lastSubRegion) {
                    $this->assertEquals(null, $lastSubRegion->getLabel());
                    $this->assertCount(1, $lastSubRegion->getWineList());

                    $lastWine = $this->getLastElementOf($lastSubRegion->getWineList());
                    if ($lastWine)
                    {
                        $this->assertEquals(
                            new Wine(
                                'Côtes de Provence « L’Excellence »',
                                'Château Saint Maur',
                                '2018',
                                36.0,
                                43.2,
                                'Rosé of a great freshness with melon and grapefruit flavours.'
                            ),
                            $lastWine
                        );
                    }
                }
            }
        }
    }

    public function testMergeLoad()
    {
        $loader = self::bootKernel()->getContainer()->get(Loader::class);
        $colorList = $loader->load([$this->getResourcesDir().'/WineMenu/Carte des vins DEF 20200610.xlsx', $this->getResourcesDir().'/WineMenu/Carte des vins DEF 20200610_custom.xlsx'], 'fr', 20);
        $this->assertContainsOnly(Color::class, $colorList);
        $this->assertCount(4, $colorList);

        /** @var Color $firstColor */
        $firstColor = $colorList[0] ?? null;
        if ($firstColor) {
            $this->assertEquals('VINS ROUGES', $firstColor->getLabel());
            $this->assertCount(8, $firstColor->getRegionList());

            $seventhRegion = $firstColor->getRegionList()[6] ?? null;
            if ($seventhRegion) {
                $this->assertEquals('LE BORDELAIS', $seventhRegion->getLabel());
                $this->assertCount(10, $seventhRegion->getSubRegionList());

                $firstSubRegion = $seventhRegion->getSubRegionList()[0];
                if ($firstSubRegion) {
                    $this->assertEquals('Haut-Médoc', $firstSubRegion->getLabel());
                    $this->assertCount(3, $firstSubRegion->getWineList());

                    $secondWine = $firstSubRegion->getWineList()[1];
                    if ($secondWine)
                    {
                        $this->assertEquals(
                            new Wine(
                                'Autre vin rouge du médoc',
                                null,
                                '2015',
                                44.0,
                                52.8,
                                'Arômes de fruits rouges et de réglisse. Corps moyen mais vin expressif.'
                            ),
                            $secondWine
                        );
                    }
                }
            }
        }

        $colorList = $loader->load([$this->getResourcesDir().'/WineMenu/Carte des vins DEF 20200610.xlsx', $this->getResourcesDir().'/WineMenu/Carte des vins DEF 20200610_custom.xlsx'], 'en', 20);
        $this->assertContainsOnly(Color::class, $colorList);
        $this->assertCount(5, $colorList);

        /** @var Color $lastColor */
        $lastColor = $this->getLastElementOf($colorList);
        if ($lastColor) {
            $this->assertEquals('Other color', $lastColor->getLabel());
            $this->assertCount(1, $lastColor->getRegionList());

            $lastRegion = $this->getLastElementOf($lastColor->getRegionList());
            if ($lastRegion) {
                $this->assertEquals('Infostrates', $lastRegion->getLabel());
                $this->assertCount(1, $lastRegion->getSubRegionList());

                $firstSubRegion = $this->getLastElementOf($lastRegion->getSubRegionList());
                if ($firstSubRegion) {
                    $this->assertEquals(null, $firstSubRegion->getLabel());
                    $this->assertCount(1, $firstSubRegion->getWineList());

                    $lastWine = $this->getLastElementOf($firstSubRegion->getWineList());
                    if ($lastWine)
                    {
                        $this->assertEquals(
                            new Wine(
                                'Vin de test',
                                'Infostrates',
                                '2020',
                                42.0,
                                50.4,
                                'This is a test.'
                            ),
                            $lastWine
                        );
                    }
                }
            }
        }
    }
}
