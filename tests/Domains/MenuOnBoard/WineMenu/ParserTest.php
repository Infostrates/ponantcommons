<?php

namespace Tests\Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions\InvalidFile;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\FrenchEnglishTextValue;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\LineParseError;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\InputLine;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Parser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Tests\Infostrates\PonantWebservicesCommons\Traits\GetResourcesDir;

class ParserTest extends TestCase
{
    use GetResourcesDir;

    /**
     * @param SplFileInfo $fileInfo
     * @dataProvider allFilesProvider
     * @throws InvalidFile
     */
    public function testAllFiles(SplFileInfo $fileInfo): void
    {
        $parser = new Parser();
        $inputLineList = $parser->parseFile($fileInfo->getPathname());
        $this->assertContainsOnlyInstancesOf(InputLine::class, $inputLineList);
    }

    public function testParseFile(): void
    {
        $parser = new Parser();
        $inputLineList = $parser->parseFile($this->getResourcesDir() . '/WineMenu/Carte des vins DEF 20200610.xlsx');
        $this->assertCount(133, $inputLineList);
        $this->assertContainsOnlyInstancesOf(InputLine::class, $inputLineList);

        $firstInputLine = $inputLineList[0] ?? null;
        if ($firstInputLine) {
            $this->assertEquals(
                new InputLine(
                    new FrenchEnglishTextValue('VINS ROUGES', 'RED WINE'),
                    new FrenchEnglishTextValue('LE BORDELAIS', 'BORDEAUX'),
                    new FrenchEnglishTextValue('Haut-Médoc', 'Haut Médoc'),
                    new FrenchEnglishTextValue('Château Lamothe Bergeron Cru Bourgeois', 'Château Lamothe Bergeron Cru Bourgeois'),
                    '',
                    '2015',
                    34.0,
                    new FrenchEnglishTextValue('Arômes de fruits rouges et de réglisse. Corps moyen mais vin expressif.', 'Medium-body red wine with red fruits and liquorice flavours.')
                ),
                $firstInputLine
            );
        }

        $lastInputLine = end($inputLineList);
        if ($lastInputLine) {
            $this->assertEquals(
                new InputLine(
                    new FrenchEnglishTextValue('VINS ROUGES', 'RED WINE'),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('Vin rouge de croisière', 'Red cruise wine'),
                    '',
                    '',
                    0.0,
                    new FrenchEnglishTextValue('', '')
                ),
                $lastInputLine
            );
        }

        $lineParseErrorList = $parser->getLastLineParseError();
        $this->assertCount(0, $lineParseErrorList);
    }

    public function testParseMergedFile(): void
    {
        $parser = new Parser();
        $inputLineList = $parser->parseFile([$this->getResourcesDir() . '/WineMenu/Carte des vins DEF 20200610.xlsx', $this->getResourcesDir() . '/WineMenu/Carte des vins DEF 20200610_custom.xlsx']);
        $this->assertCount(135, $inputLineList);
        $this->assertContainsOnlyInstancesOf(InputLine::class, $inputLineList);

        $firstInputLine = $inputLineList[0] ?? null;
        if ($firstInputLine) {
            $this->assertEquals(
                new InputLine(
                    new FrenchEnglishTextValue('VINS ROUGES', 'RED WINE'),
                    new FrenchEnglishTextValue('LE BORDELAIS', 'BORDEAUX'),
                    new FrenchEnglishTextValue('Haut-Médoc', 'Haut Médoc'),
                    new FrenchEnglishTextValue('Château Lamothe Bergeron Cru Bourgeois', 'Château Lamothe Bergeron Cru Bourgeois'),
                    '',
                    '2015',
                    34.0,
                    new FrenchEnglishTextValue('Arômes de fruits rouges et de réglisse. Corps moyen mais vin expressif.', 'Medium-body red wine with red fruits and liquorice flavours.')
                ),
                $firstInputLine
            );
        }

        $lastInputLine = end($inputLineList);
        if ($lastInputLine) {
            $this->assertEquals(
                new InputLine(
                    new FrenchEnglishTextValue('Autre couleur', 'Other color'),
                    new FrenchEnglishTextValue('Infostrates', 'Infostrates'),
                    new FrenchEnglishTextValue('', ''),
                    new FrenchEnglishTextValue('Vin de test', 'Vin de test'),
                    'Infostrates',
                    2020,
                    42.0,
                    new FrenchEnglishTextValue('Ceci est un test.', 'This is a test.')
                ),
                $lastInputLine
            );
        }

        $lineParseErrorList = $parser->getLastLineParseError();
        $this->assertCount(0, $lineParseErrorList);
    }

    public function testParseInvalidFile(): void
    {
        $this->expectExceptionObject(new InvalidFile('The row of column titles (A3:P3) does not correspond to the expected titles.'));
        $parser = new Parser();
        $parser->parseFile($this->getResourcesDir() . '/BarMenu/Carte des bars DEF 20200610.xlsx');
    }

    public static function allFilesProvider(): array
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__ . '/../../../Resources/WineMenu');

        $dataList = [];
        foreach ($finder as $file) {
            $dataList[] = [$file];
        }

        return $dataList;
    }
}
