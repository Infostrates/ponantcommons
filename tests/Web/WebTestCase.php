<?php

declare(strict_types=1);

namespace Tests\Infostrates\PonantWebservicesCommons\Web;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

class WebTestCase extends BaseWebTestCase
{
    protected static function getKernelClass(): string
    {
        return AppKernel::class;
    }
}
