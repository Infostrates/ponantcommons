<?php

declare(strict_types=1);

namespace Tests\Infostrates\PonantWebservicesCommons\Web;

use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

final class AppKernel extends Kernel
{

    public function registerBundles(): array
    {
        $bundles = [
            new \Infostrates\PonantWebservicesCommons\InfostratesPonantWebservicesCommonsBundle(),
        ];

        if($this->getEnvironment() === 'dev') {
            $bundles[] = new FrameworkBundle();
            $bundles[] = new TwigBundle();
        }
        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        if ($this->getEnvironment() === 'dev') {
            $loader->load(__DIR__.'/Resources/config/config.yml');
        }
    }

    public function getCacheDir(): string
    {
        return __DIR__ . '/var/cache/' . $this->environment;
    }

    public function getLogDir(): string
    {
        return __DIR__ . '/var/logs';
    }
}
