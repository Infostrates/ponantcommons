<?php

declare(strict_types=1);

namespace Tests\Infostrates\PonantWebservicesCommons\Web\Actions\MenuOnBoard;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Loader;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * @Route("preview/spa_menu/{language}", name="preview_spa_menu")
 */
final class PreviewSpaMenu
{
    /** @var Loader */
    private $loader;

    /** @var Environment */
    private $twig;

    /** @var string */
    private $projectDir;

    /**
     * @param Loader      $loader
     * @param Environment $twig
     * @param string      $projectDir
     */
    public function __construct(Loader $loader, Environment $twig, string $projectDir)
    {
        $this->loader = $loader;
        $this->twig = $twig;
        $this->projectDir = $projectDir;
    }

    public function __invoke(string $language)
    {
        $objectList = $this->loader->load($this->projectDir.'/tests/Resources/SpaMenu/Carte du spa DEF 20200610.xlsx', $language);

        return new Response($this->twig->render('@test_views/preview.html.twig', [
            'targetTemplate' => '@PonantWebservicesCommons/menu_on_board/preview/spa_menu.html.twig',
            'objectList' => $objectList,
        ]));
    }
}
