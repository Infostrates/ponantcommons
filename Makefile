include .env
export $(shell sed 's/=.*//' .env)

DOCKER_COMPOSE = docker-compose
DOCKER = docker
DOCKER_PHP = $(DOCKER) exec -it $(CONTAINER_NAME)_php-fpm sh -c


##
## ALIAS
## -------
##

cs: phpcs
stan: phpstan
cbf: phpcbf
quality: phpcs phpcbf
test: phpstan phpunit phpcs
pp: vendor-update


##
## Project
## -------
##

.DEFAULT_GOAL := help

help: ## Default goal (display the help message)
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-20s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

.PHONY: help

##
## Docker
## -------
##
start: ## Start environnement docker.
start: docker-compose.yml
	$(DOCKER_COMPOSE) up -d --build

init: ## Initialize project
init:
	make start
	make vendor-update


destroy: ## Destroy all containers & network
destroy:
	$(DOCKER_COMPOSE) down

stop: ## Stop all containersv related project
stop:
	$(DOCKER_COMPOSE) stop

list-containers: ## List container docker
list-containers:
	$(DOCKER_COMPOSE) ps

list-network: ## List all networks on host
list-network:
	$(DOCKER) network ls

inspect-network: ## Inspect current network to list all container ips
inspect-network:
	$(DOCKER) network inspect $(CONTAINER_NAME)

erase-all: ## Careful, erase all container, all images
erase-all:
	$(DOCKER) stop $$(docker ps -a -q) && $(DOCKER) rm $$(docker ps -a -q) $(DOCKER) rmi $$(docker images -a -q) -f

exec-php: ## Exec command inside container php. Use argument ARGS
exec-php:
	$(DOCKER_PHP) "${ARGS}"

connect-php: ## Connect sh to container php
connect-php:
	$(DOCKER) exec -u $(USER_DOCKER) -it $(CONTAINER_NAME)_php-fpm sh

stop-all: ## Stop all containers
stop-all:
	$(DOCKER) stop $$(docker ps -a -q)

##
## Manage dependencies
## -------
##

vendor: ## Install composer dependencies
vendor: composer.lock
	$(DOCKER_PHP) "composer install"

vendor-update: ## Update all composer dependencies
vendor-update: composer.json
	$(DOCKER_PHP) "composer update ${ARGS}"

vendor-require: ## Add dependency or dev dependency. Use argument ARGS (Example : make vendor-require ARGS="security form" or make vendor-require ARGS="profiler --dev"
vendor-require: composer.json
	$(DOCKER_PHP) "composer require ${ARGS}"

dump-autoload: ## Optimize autoloading and vendor
dump-autoload: composer.lock
	$(DOCKER_PHP) "composer dump-autoload"

##
## Tools
## -------
##

phpcs: ## Run phpcs
phpcs: vendor/bin/phpcs
	$(DOCKER_PHP) "vendor/bin/phpcs"

phpstan: ## Run phpstan
phpstan: vendor/bin/phpstan
	$(DOCKER_PHP) "vendor/bin/phpstan analyze src"

phpcbf: ## Run PHPCBF
phpcbf: vendor/bin/phpcbf
	$(DOCKER_PHP) vendor/bin/phpcbf

##
## TESTS
## ------
##
phpunit: ## Run phpunit
phpunit: tests
	$(DOCKER_PHP) "vendor/bin/phpunit"
