<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfostratesPonantWebservicesCommonsBundle extends Bundle
{
}
