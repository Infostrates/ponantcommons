<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard;

final class LineParseError
{
    /** @var int */
    private $line;

    /** @var string */
    private $message;

    /**
     * @param int    $line
     * @param string $message
     */
    public function __construct(int $line, string $message)
    {
        $this->line = $line;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
