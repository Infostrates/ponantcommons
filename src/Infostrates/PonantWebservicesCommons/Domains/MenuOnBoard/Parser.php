<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions\InvalidFile;
use InvalidArgumentException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Throwable;

abstract class Parser
{
    /** @var LineParseError[] */
    private $lastLineParseError;

    /**
     * @param string|string[] $fileNames
     * @return object[]
     * @throws InvalidFile
     */
    public function parseFile($fileNames): array
    {
        try {
            $fileNames = is_array($fileNames) ? $fileNames : [$fileNames];

            $this->lastLineParseError = [];

            $linesList = [];
            foreach ($fileNames as $filename) {
                $spreadSheet = $this->getSpreadSheet($filename);
                $sheet = $this->getSheet($spreadSheet);
                $this->checkSheetValidity($sheet);
                $linesList[] = $this->parseSheet($sheet);
            }

            return array_merge([], ...$linesList);
        } catch (Exception $e) {
            throw new InvalidFile($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @return LineParseError[]
     */
    public function getLastLineParseError(): array
    {
        return $this->lastLineParseError;
    }

    /**
     * @param string $fileName
     * @return Spreadsheet
     * @throws Exception
     */
    private function getSpreadSheet(string $fileName): Spreadsheet
    {
        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);

        return $reader->load($fileName);
    }

    /**
     * @param Worksheet $worksheet
     * @return object[]
     */
    private function parseSheet(Worksheet $worksheet): array
    {
        $rows = $worksheet->toArray('', true, false, true);
        $inputLineList = [];
        foreach ($rows as $lineNumber => $row) {
            try {
                if ($lineNumber < $this->getDataStartLine()) {
                    continue;
                }

                $inputLineList[] = $this->parseRow($row);
            } catch (Throwable $e) {
                $this->addLineParseError(new LineParseError($lineNumber, $e->getMessage()));
            }
        }

        return $inputLineList;
    }

    /**
     * @param Spreadsheet $spreadSheet
     * @return Worksheet
     * @throws InvalidFile
     */
    private function getSheet(Spreadsheet $spreadSheet): Worksheet
    {
        try {
            return $spreadSheet->getSheet(0);
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            throw new InvalidFile('Unable to read the first worksheet', $e->getCode(), $e);
        }
    }

    /**
     * @param array<string, mixed> $row
     * @param string               $frenchColumnLetter
     * @param string               $englishColumnLetter
     * @param bool                 $isRequired
     * @return FrenchEnglishTextValue
     */
    protected function getFrenchEnglishTextValueForColumns(array $row, string $frenchColumnLetter, string $englishColumnLetter, bool $isRequired = false): FrenchEnglishTextValue
    {
        $value = new FrenchEnglishTextValue($this->getTextValueForColumn($row, $frenchColumnLetter), $this->getTextValueForColumn($row, $englishColumnLetter));

        if (array_filter($row)) {
            if ($isRequired && $value->isEmpty()) {
                throw new InvalidArgumentException(sprintf("Columns %s and %s are mandatory", $frenchColumnLetter, $englishColumnLetter));
            }
        }

        return $value;
    }

    /**
     * @param array<string, mixed> $row
     * @param string               $columnLetter
     * @param bool                 $isRequired
     * @return string
     */
    protected function getTextValueForColumn(array $row, string $columnLetter, bool $isRequired = false): string
    {
        $value = trim((string)($row[$columnLetter] ?? ''));

        if ($isRequired && empty($value)) {
            throw new InvalidArgumentException(sprintf("Column %s is mandatory", $columnLetter));
        }

        return $value;
    }

    /**
     * @param array<string, mixed> $row
     * @param string               $columnLetter
     * @param bool                 $isRequired
     * @return float
     */
    protected function getFloatValueFromColumn(array $row, string $columnLetter, bool $isRequired = false): float
    {
        $value = (float)str_replace([' ', ','], ['', '.'], $this->getTextValueForColumn($row, $columnLetter));

        if ($isRequired && empty($value)) {
            throw new InvalidArgumentException(sprintf("Column %s is mandatory", $columnLetter));
        }

        return $value;
    }

    /**
     * @param array<string, mixed> $row
     * @param string               $columnLetter
     * @return bool
     */
    public function getBoolValueFromColumn(array $row, string $columnLetter): bool
    {
        return !empty($this->getTextValueForColumn($row, $columnLetter));
    }

    /**
     * @param LineParseError $lineParseError
     */
    protected function addLineParseError(LineParseError $lineParseError): void
    {
        $this->lastLineParseError[] = $lineParseError;
    }

    /**
     * @param Worksheet $worksheet
     * @throws InvalidFile
     */
    abstract protected function checkSheetValidity(Worksheet $worksheet): void;

    /**
     * @param array<string, mixed> $row
     * @return object
     */
    abstract protected function parseRow(array $row);

    /**
     * @return int
     */
    abstract protected function getDataStartLine(): int;
}
