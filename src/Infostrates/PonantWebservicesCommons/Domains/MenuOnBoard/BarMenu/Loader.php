<?php

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\Drink;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\Type1;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\Type2;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\Type3;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Loader as AbstractLoader;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\InputLine;

final class Loader extends AbstractLoader
{
    public function __construct(Parser $parser)
    {
        parent::__construct($parser);
    }

    /**
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage VAT (in %) to apply on price without taxes
     * @return Type1[]
     */
    protected function loadFromInputLineList(array $inputLineList, string $language, float $vatPercentage): array
    {
        $inputLineByType1Name = [];
        foreach ($inputLineList as $inputLine) {
            $type1Name = (string)$this->loadValue($inputLine->getType1(), $language);
            if (!isset($inputLineByType1Name[$type1Name])) {
                $inputLineByType1Name[$type1Name] = [];
            }
            $inputLineByType1Name[$type1Name][] = $inputLine;
        }

        $type1List = [];
        foreach ($inputLineByType1Name as $type1Name => $subInputLineList) {
            $type1List[] = $this->loadType1($type1Name, $subInputLineList, $language, $vatPercentage);
        }

        return $type1List;
    }

    /**
     * @param string      $type1Name
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return Type1
     */
    private function loadType1(string $type1Name, array $inputLineList, string $language, float $vatPercentage): Type1
    {
        $inputLineByType2Name = [];
        foreach ($inputLineList as $inputLine) {
            $type2Name = (string)$this->loadValue($inputLine->getType2(), $language);
            if (!isset($inputLineByType2Name[$type2Name])) {
                $inputLineByType2Name[$type2Name] = [];
            }
            $inputLineByType2Name[$type2Name][] = $inputLine;
        }

        ksort($inputLineByType2Name);

        $type2List = [];
        foreach ($inputLineByType2Name as $type2Name => $subInputLineList) {
            $type2List[] = $this->loadType2($type2Name, $subInputLineList, $language, $vatPercentage);
        }

        return new Type1($type1Name, $type2List);
    }

    /**
     * @param string      $regionName
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return Type2
     */
    private function loadType2(string $regionName, array $inputLineList, string $language, float $vatPercentage): Type2
    {
        $inputLineByType3Name = [];
        foreach ($inputLineList as $inputLine) {
            $type3Name = (string)$this->loadValue($inputLine->getType3(), $language);
            if (!isset($inputLineByType3Name[$type3Name])) {
                $inputLineByType3Name[$type3Name] = [];
            }
            $inputLineByType3Name[$type3Name][] = $inputLine;
        }

        $type3List = [];
        foreach ($inputLineByType3Name as $type3Name => $subInputLineList) {
            $type3List[] = $this->loadType3($type3Name, $subInputLineList, $language, $vatPercentage);
        }

        return new Type2($regionName, $type3List);
    }

    /**
     * @param string      $type3Name
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return Type3
     */
    private function loadType3(string $type3Name, array $inputLineList, string $language, float $vatPercentage): Type3
    {
        usort($inputLineList, static function (InputLine $inputLineA, InputLine $inputLineB) {
            return $inputLineA->getPriceWithoutTaxes() <=> $inputLineB->getPriceWithoutTaxes();
        });

        $drinkList = [];
        foreach ($inputLineList as $inputLine) {
            $drinkList[] = $this->loadDrink($inputLine, $language, $vatPercentage);
        }

        return new Type3($type3Name ?: null, $drinkList);
    }

    private function loadDrink(InputLine $inputLine, string $language, float $vatPercentage): Drink
    {
        $ingredientValue = $this->loadValue($inputLine->getIngredients(), $language);

        return new Drink(
            $this->loadValue($inputLine->getName(), $language),
            $ingredientValue ?: null,
            $this->loadValue($inputLine->getPriceWithoutTaxes() ?: null, $language),
            $inputLine->getPriceWithoutTaxes() ? $this->loadValueAndApplyVat($inputLine->getPriceWithoutTaxes(), $language, $vatPercentage) : null,
            $this->loadValue($inputLine->isIncludedInPassBarPremium(), $language)
        );
    }
}
