<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions\InvalidFile;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Parser as AbstractParser;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models\InputLine;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

final class Parser extends AbstractParser
{
    /**
     * @param Worksheet $worksheet
     * @throws InvalidFile
     */
    protected function checkSheetValidity(Worksheet $worksheet): void
    {
        $titleLineRange = 'A3:M3';
        $titleLine = $worksheet->rangeToArray($titleLineRange, '', true, false)[0];

        $cleanedTitleLine = array_map(
            static function ($value) {
                return trim($value);
            },
            $titleLine
        );

        if (
            $cleanedTitleLine !== [
                'TYPE 1 FR',
                'TYPE 1 EN',
                'TYPE 2 FR',
                'TYPE 2 EN',
                'TYPE 3 FR',
                'TYPE 3 EN',
                'Nom FR',
                'Nom EN',
                'Ingrédients Fr',
                'Ingrédients EN',
                'Prix HT',
                'Prix TTC',
                'Inclus dans pass bar premium',
            ]
        ) {
            throw new InvalidFile('The row of column titles (' . $titleLineRange . ') does not correspond to the expected titles.');
        }
    }

    /**
     * @param array<string, mixed> $row
     * @return InputLine
     */
    protected function parseRow(array $row): InputLine
    {
        return new InputLine(
            $this->getFrenchEnglishTextValueForColumns($row, 'A', 'B', true),
            $this->getFrenchEnglishTextValueForColumns($row, 'C', 'D'),
            $this->getFrenchEnglishTextValueForColumns($row, 'E', 'F'),
            $this->getFrenchEnglishTextValueForColumns($row, 'G', 'H', true),
            $this->getFrenchEnglishTextValueForColumns($row, 'I', 'J'),
            $this->getFloatValueFromColumn($row, 'K'),
            $this->getBoolValueFromColumn($row, 'M')
        );
    }

    protected function getDataStartLine(): int
    {
        return 4;
    }
}
