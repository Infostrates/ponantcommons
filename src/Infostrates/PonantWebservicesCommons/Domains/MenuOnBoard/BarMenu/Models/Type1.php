<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models;

class Type1
{
    /** @var string */
    private $label;

    /** @var Type2[] */
    private $type2List;

    /**
     * @param string $label
     * @param Type2[]     $type2List
     */
    public function __construct(string $label, array $type2List)
    {
        $this->label = $label;
        $this->type2List = $type2List;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return Type2[]
     */
    public function getType2List(): array
    {
        return $this->type2List;
    }
}
