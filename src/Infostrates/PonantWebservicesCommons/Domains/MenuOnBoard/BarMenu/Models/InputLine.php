<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\FrenchEnglishTextValue;

final class InputLine
{
    /** @var FrenchEnglishTextValue */
    private $type1;

    /** @var FrenchEnglishTextValue */
    private $type2;

    /** @var FrenchEnglishTextValue */
    private $type3;

    /** @var FrenchEnglishTextValue */
    private $name;

    /** @var FrenchEnglishTextValue */
    private $ingredients;

    /** @var float */
    private $priceWithoutTaxes;

    /** @var bool */
    private $includedInPassBarPremium;

    /**
     * @param FrenchEnglishTextValue $type1
     * @param FrenchEnglishTextValue $type2
     * @param FrenchEnglishTextValue $type3
     * @param FrenchEnglishTextValue $name
     * @param FrenchEnglishTextValue $ingredients
     * @param float                  $priceWithoutTaxes
     * @param bool                   $includedInPassBarPremium
     */
    public function __construct(FrenchEnglishTextValue $type1, FrenchEnglishTextValue $type2, FrenchEnglishTextValue $type3, FrenchEnglishTextValue $name, FrenchEnglishTextValue $ingredients, float $priceWithoutTaxes, bool $includedInPassBarPremium)
    {
        $this->type1 = $type1;
        $this->type2 = $type2;
        $this->type3 = $type3;
        $this->name = $name;
        $this->ingredients = $ingredients;
        $this->priceWithoutTaxes = $priceWithoutTaxes;
        $this->includedInPassBarPremium = $includedInPassBarPremium;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getType1(): FrenchEnglishTextValue
    {
        return $this->type1;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getType2(): FrenchEnglishTextValue
    {
        return $this->type2;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getType3(): FrenchEnglishTextValue
    {
        return $this->type3;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getName(): FrenchEnglishTextValue
    {
        return $this->name;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getIngredients(): FrenchEnglishTextValue
    {
        return $this->ingredients;
    }

    /**
     * @return float
     */
    public function getPriceWithoutTaxes(): float
    {
        return $this->priceWithoutTaxes;
    }

    /**
     * @return bool
     */
    public function isIncludedInPassBarPremium(): bool
    {
        return $this->includedInPassBarPremium;
    }
}
