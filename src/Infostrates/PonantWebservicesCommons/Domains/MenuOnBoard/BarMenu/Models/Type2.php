<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models;

class Type2
{
    /** @var string|null Can be null */
    private $label;

    /** @var Type3[] */
    private $type3List;

    /**
     * @param string|null $label
     * @param Type3[]     $type3List
     */
    public function __construct(?string $label, array $type3List)
    {
        $this->label = $label;
        $this->type3List = $type3List;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return Type3[]
     */
    public function getType3List(): array
    {
        return $this->type3List;
    }
}
