<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models;

class Drink
{
    /** @var string */
    private $name;

    /** @var string|null Can be null */
    private $ingredients;

    /** @var float|null Can be null */
    private $priceWithoutTaxes;

    /** @var float|null Can be null */
    private $price;

    /** @var bool */
    private $includedInPassBarPremium;

    /**
     * @param string      $name
     * @param string|null $ingredients
     * @param float|null  $priceWithoutTaxes
     * @param float|null  $price
     * @param bool        $includedInPassBarPremium
     */
    public function __construct(string $name, ?string $ingredients, ?float $priceWithoutTaxes, ?float $price, bool $includedInPassBarPremium)
    {
        $this->name = $name;
        $this->ingredients = $ingredients;
        $this->priceWithoutTaxes = $priceWithoutTaxes;
        $this->price = $price;
        $this->includedInPassBarPremium = $includedInPassBarPremium;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getIngredients(): ?string
    {
        return $this->ingredients;
    }

    /**
     * @return float|null
     */
    public function getPriceWithoutTaxes(): ?float
    {
        return $this->priceWithoutTaxes;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function isIncludedInPassBarPremium(): bool
    {
        return $this->includedInPassBarPremium;
    }
}
