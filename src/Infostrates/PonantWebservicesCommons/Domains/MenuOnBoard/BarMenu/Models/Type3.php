<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\BarMenu\Models;

class Type3
{
    /** @var string|null Can be null */
    private $label;

    /** @var Drink[] */
    private $drinkList;

    /**
     * @param string|null $label
     * @param Drink[]     $drinkList
     */
    public function __construct(?string $label, array $drinkList)
    {
        $this->label = $label;
        $this->drinkList = $drinkList;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return Drink[]
     */
    public function getDrinkList(): array
    {
        return $this->drinkList;
    }
}
