<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models;

class Region
{
    /** @var string|null Can be null */
    private $label;

    /** @var SubRegion[] */
    private $subRegionList;

    /**
     * @param string|null $label
     * @param SubRegion[] $subRegionList
     */
    public function __construct(?string $label, array $subRegionList)
    {
        $this->label = $label;
        $this->subRegionList = $subRegionList;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return SubRegion[]
     */
    public function getSubRegionList(): array
    {
        return $this->subRegionList;
    }
}
