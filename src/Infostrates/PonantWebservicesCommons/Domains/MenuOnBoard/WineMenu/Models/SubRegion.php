<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models;

class SubRegion
{
    /** @var string|null Can be null */
    private $label;

    /** @var Wine[] */
    private $wineList;

    /**
     * @param string|null $label
     * @param Wine[]      $wineList
     */
    public function __construct(?string $label, array $wineList)
    {
        $this->label = $label;
        $this->wineList = $wineList;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return Wine[]
     */
    public function getWineList(): array
    {
        return $this->wineList;
    }
}
