<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\FrenchEnglishTextValue;

final class InputLine
{
    /** @var FrenchEnglishTextValue */
    private $color;

    /** @var FrenchEnglishTextValue */
    private $region;

    /** @var FrenchEnglishTextValue */
    private $subRegion;

    /** @var FrenchEnglishTextValue */
    private $name;

    /** @var string */
    private $domain;

    /** @var string */
    private $year;

    /** @var float */
    private $priceWithoutTaxes;

    /** @var FrenchEnglishTextValue */
    private $description;

    /**
     * @param FrenchEnglishTextValue $color
     * @param FrenchEnglishTextValue $region
     * @param FrenchEnglishTextValue $subRegion
     * @param FrenchEnglishTextValue $name
     * @param string                 $domain
     * @param string                 $year
     * @param float                  $priceWithoutTaxes
     * @param FrenchEnglishTextValue $description
     */
    public function __construct(FrenchEnglishTextValue $color, FrenchEnglishTextValue $region, FrenchEnglishTextValue $subRegion, FrenchEnglishTextValue $name, string $domain, string $year, float $priceWithoutTaxes, FrenchEnglishTextValue $description)
    {
        $this->color = $color;
        $this->region = $region;
        $this->subRegion = $subRegion;
        $this->name = $name;
        $this->domain = $domain;
        $this->year = $year;
        $this->priceWithoutTaxes = $priceWithoutTaxes;
        $this->description = $description;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getColor(): FrenchEnglishTextValue
    {
        return $this->color;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getRegion(): FrenchEnglishTextValue
    {
        return $this->region;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getSubRegion(): FrenchEnglishTextValue
    {
        return $this->subRegion;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getName(): FrenchEnglishTextValue
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getYear(): string
    {
        return $this->year;
    }

    /**
     * @return float
     */
    public function getPriceWithoutTaxes(): float
    {
        return $this->priceWithoutTaxes;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getDescription(): FrenchEnglishTextValue
    {
        return $this->description;
    }
}
