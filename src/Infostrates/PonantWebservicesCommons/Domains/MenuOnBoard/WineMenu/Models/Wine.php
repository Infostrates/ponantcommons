<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models;

class Wine
{
    /** @var string */
    private $name;

    /** @var string|null Can be null */
    private $domain;

    /** @var string|null Can be null */
    private $year;

    /** @var float|null Can be null */
    private $priceWithoutTaxes;

    /** @var float|null Can be null */
    private $price;

    /** @var string|null Can be null */
    private $description;

    /**
     * @param string      $name
     * @param string|null $domain
     * @param string|null $year
     * @param float|null  $priceWithoutTaxes
     * @param float|null  $price
     * @param string|null $description
     */
    public function __construct(string $name, ?string $domain, ?string $year, ?float $priceWithoutTaxes, ?float $price, ?string $description)
    {
        $this->name = $name;
        $this->domain = $domain;
        $this->year = $year;
        $this->priceWithoutTaxes = $priceWithoutTaxes;
        $this->price = $price;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @return string|null
     */
    public function getYear(): ?string
    {
        return $this->year;
    }

    /**
     * @return float|null
     */
    public function getPriceWithoutTaxes(): ?float
    {
        return $this->priceWithoutTaxes;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
}
