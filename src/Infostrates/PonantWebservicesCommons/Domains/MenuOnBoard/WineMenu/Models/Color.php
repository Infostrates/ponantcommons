<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models;

class Color
{
    /** @var string|null Can be null */
    private $label;

    /** @var Region[] */
    private $regionList;

    /**
     * @param string|null $label
     * @param Region[]    $regionList
     */
    public function __construct(?string $label, array $regionList)
    {
        $this->label = $label;
        $this->regionList = $regionList;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return Region[]
     */
    public function getRegionList(): array
    {
        return $this->regionList;
    }
}
