<?php

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Loader as AbstractLoader;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\Color;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\InputLine;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\Region;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\SubRegion;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\Wine;

final class Loader extends AbstractLoader
{
    public function __construct(Parser $parser)
    {
        parent::__construct($parser);
    }

    /**
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage VAT (in %) to apply on price without taxes
     * @return Color[]
     */
    protected function loadFromInputLineList(array $inputLineList, string $language, float $vatPercentage): array
    {
        $inputLineByColorName = [];
        foreach ($inputLineList as $inputLine) {
            $colorName = (string)$this->loadValue($inputLine->getColor(), $language);
            if (!isset($inputLineByColorName[$colorName])) {
                $inputLineByColorName[$colorName] = [];
            }
            $inputLineByColorName[$colorName][] = $inputLine;
        }

        $colorList = [];
        foreach ($inputLineByColorName as $colorName => $subInputLineList) {
            $colorList[] = $this->loadColor($colorName, $subInputLineList, $language, $vatPercentage);
        }

        return $colorList;
    }

    /**
     * @param string      $colorName
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return Color
     */
    private function loadColor(string $colorName, array $inputLineList, string $language, float $vatPercentage): Color
    {
        $inputLineByRegionName = [];
        foreach ($inputLineList as $inputLine) {
            $regionName = (string)$this->loadValue($inputLine->getRegion(), $language);
            if (!isset($inputLineByRegionName[$regionName])) {
                $inputLineByRegionName[$regionName] = [];
            }
            $inputLineByRegionName[$regionName][] = $inputLine;
        }

        ksort($inputLineByRegionName);

        $regionList = [];
        foreach ($inputLineByRegionName as $regionName => $subInputLineList) {
            $regionList[] = $this->loadRegion($regionName, $subInputLineList, $language, $vatPercentage);
        }

        return new Color($colorName, $regionList);
    }

    /**
     * @param string      $regionName
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return Region
     */
    private function loadRegion(string $regionName, array $inputLineList, string $language, float $vatPercentage): Region
    {
        $inputLineBySubRegionName = [];
        foreach ($inputLineList as $inputLine) {
            $subRegionName = (string)$this->loadValue($inputLine->getSubRegion(), $language);
            if (!isset($inputLineBySubRegionName[$subRegionName])) {
                $inputLineBySubRegionName[$subRegionName] = [];
            }
            $inputLineBySubRegionName[$subRegionName][] = $inputLine;
        }

        ksort($inputLineBySubRegionName);

        $subRegionList = [];
        foreach ($inputLineBySubRegionName as $subRegionName => $subInputLineList) {
            $subRegionList[] = $this->loadSubRegion($subRegionName, $subInputLineList, $language, $vatPercentage);
        }

        return new Region($regionName ?: null, $subRegionList);
    }

    /**
     * @param string      $subRegionName
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return SubRegion
     */
    private function loadSubRegion(string $subRegionName, array $inputLineList, string $language, float $vatPercentage): SubRegion
    {
        usort($inputLineList, static function (InputLine $inputLineA, InputLine $inputLineB) {
            return $inputLineA->getPriceWithoutTaxes() <=> $inputLineB->getPriceWithoutTaxes();
        });

        $wineList = [];
        foreach ($inputLineList as $inputLine) {
            $wineList[] = $this->loadWine($inputLine, $language, $vatPercentage);
        }

        return new SubRegion($subRegionName ?: null, $wineList);
    }

    private function loadWine(InputLine $inputLine, string $language, float $vatPercentage): Wine
    {
        return new Wine(
            $this->loadValue($inputLine->getName(), $language),
            $this->loadValue($inputLine->getDomain() ?: null, $language),
            $this->loadValue($inputLine->getYear() ?: null, $language),
            $this->loadValue($inputLine->getPriceWithoutTaxes() ?: null, $language),
            $inputLine->getPriceWithoutTaxes() ? $this->loadValueAndApplyVat($inputLine->getPriceWithoutTaxes(), $language, $vatPercentage) : null,
            $this->loadValue($inputLine->getDescription(), $language)
        );
    }
}
