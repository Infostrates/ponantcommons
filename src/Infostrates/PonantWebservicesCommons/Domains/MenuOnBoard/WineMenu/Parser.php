<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions\InvalidFile;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Parser as AbstractParser;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\WineMenu\Models\InputLine;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

final class Parser extends AbstractParser
{
    /**
     * @param Worksheet $worksheet
     * @throws InvalidFile
     */
    protected function checkSheetValidity(Worksheet $worksheet): void
    {
        $titleLineRange = 'A3:P3';
        $titleLine = $worksheet->rangeToArray($titleLineRange, '', true, false)[0];

        $cleanedTitleLine = array_map(
            static function ($value) {
                return trim($value);
            },
            $titleLine
        );

        if (
            $cleanedTitleLine !== [
                'Couleur FR',
                'Couleur EN',
                'Region FR',
                'Région EN',
                'Sous région FR',
                'Sous région EN',
                'Name FR',
                'Name EN',
                'Actif 2019',
                'Domaine',
                'Année',
                'Prix HT',
                'Prix TTC',
                'Rating',
                'Descriptif FR',
                'Descriptif EN',
            ]
        ) {
            throw new InvalidFile('The row of column titles (' . $titleLineRange . ') does not correspond to the expected titles.');
        }
    }

    /**
     * @param array<string, mixed> $row
     * @return InputLine
     */
    protected function parseRow(array $row): InputLine
    {
        return new InputLine(
            $this->getFrenchEnglishTextValueForColumns($row, 'A', 'B', true),
            $this->getFrenchEnglishTextValueForColumns($row, 'C', 'D'),
            $this->getFrenchEnglishTextValueForColumns($row, 'E', 'F'),
            $this->getFrenchEnglishTextValueForColumns($row, 'G', 'H', true),
            $this->getTextValueForColumn($row, 'J'),
            $this->getTextValueForColumn($row, 'K'),
            $this->getFloatValueFromColumn($row, 'L'),
            $this->getFrenchEnglishTextValueForColumns($row, 'O', 'P')
        );
    }

    protected function getDataStartLine(): int
    {
        return 4;
    }
}
