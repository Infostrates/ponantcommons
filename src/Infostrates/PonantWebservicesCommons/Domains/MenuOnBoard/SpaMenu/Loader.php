<?php

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\Care;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\Menu;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\Type2;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Loader as AbstractLoader;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\InputLine;

final class Loader extends AbstractLoader
{
    public function __construct(Parser $parser)
    {
        parent::__construct($parser);
    }
    /**
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage VAT (in %) to apply on price without taxes
     * @return Menu[]
     */
    protected function loadFromInputLineList(array $inputLineList, string $language, float $vatPercentage): array
    {
        $inputLineByMenuName = [];
        foreach ($inputLineList as $inputLine) {
            $menuName = (string)$this->loadValue($inputLine->getMenu(), $language);
            if (!isset($inputLineByMenuName[$menuName])) {
                $inputLineByMenuName[$menuName] = [];
            }
            $inputLineByMenuName[$menuName][] = $inputLine;
        }

        $menuList = [];
        foreach ($inputLineByMenuName as $menuName => $subInputLineList) {
            /** @var InputLine|null $firstElement */
            $firstElement = ($subInputLineList[0] ?? null) ?: null;
            if ($firstElement) {
                $brandName = $firstElement->getBrand() ?: null;
            } else {
                $brandName = null;
            }
            $menuList[] = $this->loadMenu($menuName, $brandName, $subInputLineList, $language, $vatPercentage);
        }

        return $menuList;
    }

    /**
     * @param string      $menuName
     * @param string|null $brandName
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return Menu
     */
    private function loadMenu(string $menuName, ?string $brandName, array $inputLineList, string $language, float $vatPercentage): Menu
    {
        $inputLineByType2Name = [];
        foreach ($inputLineList as $inputLine) {
            $type2Name = (string)$this->loadValue($inputLine->getType2(), $language);
            if (!isset($inputLineByType2Name[$type2Name])) {
                $inputLineByType2Name[$type2Name] = [];
            }
            $inputLineByType2Name[$type2Name][] = $inputLine;
        }

        $this->placeNamelessType2OnTopOfList($inputLineByType2Name);

        $type2List = [];
        foreach ($inputLineByType2Name as $type2Name => $subInputLineList) {
            /** @var InputLine|null $firstElement */
            $firstElement = ($subInputLineList[0] ?? null) ?: null;
            if ($firstElement) {
                $type2Description = $this->loadValue($firstElement->getType2Description(), $language) ?: null;
            } else {
                $type2Description = null;
            }
            $type2List[] = $this->loadType2($type2Name, $type2Description, $subInputLineList, $language, $vatPercentage);
        }

        return new Menu($menuName, $brandName, $type2List);
    }

    /**
     * @param string      $type2Name
     * @param string|null $type2Description
     * @param InputLine[] $inputLineList
     * @param string      $language
     * @param float       $vatPercentage
     * @return Type2
     */
    private function loadType2(string $type2Name, ?string $type2Description, array $inputLineList, string $language, float $vatPercentage): Type2
    {
        $careList = [];
        foreach ($inputLineList as $inputLine) {
            $careList[] = $this->loadCare($inputLine, $language, $vatPercentage);
        }

        return new Type2($type2Name ?: null, $type2Description ?: null, $careList);
    }

    private function loadCare(InputLine $inputLine, string $language, float $vatPercentage): Care
    {
        return new Care(
            $this->loadValue($inputLine->getName(), $language),
            $this->loadValue($inputLine->getDescription(), $language) ?: null,
            $this->loadValue($inputLine->getDuration(), $language) ?: null,
            $this->loadGenderValue($inputLine->getGender(), $language) ?: null,
            $this->loadValue($inputLine->getPriceWithoutTaxes() ?: null, $language),
            $inputLine->getPriceWithoutTaxes() ? $this->loadValueAndApplyVat($inputLine->getPriceWithoutTaxes(), $language, $vatPercentage) : null
        );
    }

    /**
     * @param mixed  $value
     * @param string $language
     * @return string|null
     */
    private function loadGenderValue($value, string $language): ?string
    {
        $value = $this->loadValue($value, $language);
        switch (strtolower($value)) {
            case 'femme':
            case 'women':
            case 'woman':
                return Care::GENDER_WOMEN;
            case 'homme':
            case 'men':
            case 'man':
                return Care::GENDER_MEN;
            case 'enfant':
            case 'child':
            case 'children':
                return Care::GENDER_CHILD;
            case 'mixte':
            case 'mixed':
            case 'mix':
                return Care::GENDER_MIXED;
            default:
                return null;
        }
    }

    /**
     * @param array<string, array<InputLine>> $inputLineByType2Name
     */
    private function placeNamelessType2OnTopOfList(array &$inputLineByType2Name): void
    {
        if (isset($inputLineByType2Name[''])) {
            $inputLines = $inputLineByType2Name[''];
            unset($inputLineByType2Name['']);
            $inputLineByType2Name = array_merge(['' => $inputLines], $inputLineByType2Name);
        }
    }
}
