<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models;

class Type2
{
    /** @var string|null Can be null */
    private $label;

    /** @var string|null Can be null */
    private $description;

    /** @var Care[] */
    private $careList;

    /**
     * @param string|null $label
     * @param string|null $description
     * @param Care[]      $careList
     */
    public function __construct(?string $label, ?string $description, array $careList)
    {
        $this->label = $label;
        $this->description = $description;
        $this->careList = $careList;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return Care[]
     */
    public function getCareList(): array
    {
        return $this->careList;
    }
}
