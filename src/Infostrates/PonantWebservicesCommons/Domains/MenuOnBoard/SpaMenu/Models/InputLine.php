<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\FrenchEnglishTextValue;

final class InputLine
{
    /** @var string */
    private $brand;

    /** @var FrenchEnglishTextValue */
    private $menu;

    /** @var FrenchEnglishTextValue */
    private $type2;

    /** @var FrenchEnglishTextValue */
    private $type2Description;

    /** @var FrenchEnglishTextValue */
    private $name;

    /** @var FrenchEnglishTextValue */
    private $description;

    /** @var string */
    private $duration;

    /** @var string */
    private $gender;

    /** @var float */
    private $priceWithoutTaxes;

    /**
     * @param string                 $brand
     * @param FrenchEnglishTextValue $menu
     * @param FrenchEnglishTextValue $type2
     * @param FrenchEnglishTextValue $type2Description
     * @param FrenchEnglishTextValue $name
     * @param FrenchEnglishTextValue $description
     * @param string                 $duration
     * @param string                 $gender
     * @param float                  $priceWithoutTaxes
     */
    public function __construct(string $brand, FrenchEnglishTextValue $menu, FrenchEnglishTextValue $type2, FrenchEnglishTextValue $type2Description, FrenchEnglishTextValue $name, FrenchEnglishTextValue $description, string $duration, string $gender, float $priceWithoutTaxes)
    {
        $this->brand = $brand;
        $this->menu = $menu;
        $this->type2 = $type2;
        $this->type2Description = $type2Description;
        $this->name = $name;
        $this->description = $description;
        $this->duration = $duration;
        $this->gender = $gender;
        $this->priceWithoutTaxes = $priceWithoutTaxes;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getMenu(): FrenchEnglishTextValue
    {
        return $this->menu;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getType2(): FrenchEnglishTextValue
    {
        return $this->type2;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getType2Description(): FrenchEnglishTextValue
    {
        return $this->type2Description;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getName(): FrenchEnglishTextValue
    {
        return $this->name;
    }

    /**
     * @return FrenchEnglishTextValue
     */
    public function getDescription(): FrenchEnglishTextValue
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        return $this->duration;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @return float
     */
    public function getPriceWithoutTaxes(): float
    {
        return $this->priceWithoutTaxes;
    }
}
