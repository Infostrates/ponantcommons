<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models;

class Care
{
    public const GENDER_MEN = 'men';
    public const GENDER_WOMEN = 'women';
    public const GENDER_CHILD = 'children';
    public const GENDER_MIXED = 'mixed';

    /** @var string */
    private $name;

    /** @var string|null Can be null */
    private $description;

    /** @var string|null Can be null */
    private $duration;

    /** @var string|null Can be 'men', 'women', 'mixed', or null */
    private $gender;

    /** @var float|null Can be null */
    private $priceWithoutTaxes;

    /** @var float|null Can be null */
    private $price;

    /**
     * @param string      $name
     * @param string|null $description
     * @param string|null $duration
     * @param string|null $gender
     * @param float|null  $priceWithoutTaxes
     * @param float|null  $price
     */
    public function __construct(string $name, ?string $description, ?string $duration, ?string $gender, ?float $priceWithoutTaxes, ?float $price)
    {
        $this->name = $name;
        $this->description = $description;
        $this->duration = $duration;
        $this->gender = $gender;
        $this->priceWithoutTaxes = $priceWithoutTaxes;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getDuration(): ?string
    {
        return $this->duration;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @return float|null
     */
    public function getPriceWithoutTaxes(): ?float
    {
        return $this->priceWithoutTaxes;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }
}
