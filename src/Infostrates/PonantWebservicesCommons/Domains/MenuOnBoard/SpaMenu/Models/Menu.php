<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models;

class Menu
{
    /** @var string */
    private $label;

    /** @var string|null Can be null */
    private $brand;

    /** @var Type2[] */
    private $type2List;

    /**
     * @param string      $label
     * @param string|null $brand
     * @param Type2[]     $type2List
     */
    public function __construct(string $label, ?string $brand, array $type2List)
    {
        $this->label = $label;
        $this->brand = $brand;
        $this->type2List = $type2List;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string|null
     */
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * @return Type2[]
     */
    public function getType2List(): array
    {
        return $this->type2List;
    }
}
