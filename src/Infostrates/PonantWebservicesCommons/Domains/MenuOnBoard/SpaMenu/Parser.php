<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions\InvalidFile;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Parser as AbstractParser;
use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\SpaMenu\Models\InputLine;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

final class Parser extends AbstractParser
{
    /**
     * @param Worksheet $worksheet
     * @throws InvalidFile
     */
    protected function checkSheetValidity(Worksheet $worksheet): void
    {
        $titleLineRange = 'A2:O2';
        $titleLine = $worksheet->rangeToArray($titleLineRange, '', true, false)[0];

        $cleanedTitleLine = array_map(
            static function ($value) {
                return trim($value);
            },
            $titleLine
        );

        if (
            $cleanedTitleLine !== [
                'MARQUE',
                'MENU FR',
                'MENU EN',
                'TYPE 2 FR',
                'TYPE 2 EN',
                'Descriptif  TYPE FR',
                'Descriptif  TYPE EN',
                'Nom FR',
                'Nom EN',
                'Descriptif FR',
                'Descriptif EN',
                'Durée',
                'homme/femme/mixte',
                'Prix HT',
                'Prix TTC',
            ]
        ) {
            throw new InvalidFile('The row of column titles (' . $titleLineRange . ') does not correspond to the expected titles.');
        }
    }

    /**
     * @param array<string, mixed> $row
     * @return InputLine
     */
    protected function parseRow(array $row): InputLine
    {
        return new InputLine(
            $this->getTextValueForColumn($row, 'A'),
            $this->getFrenchEnglishTextValueForColumns($row, 'B', 'C', true),
            $this->getFrenchEnglishTextValueForColumns($row, 'D', 'E'),
            $this->getFrenchEnglishTextValueForColumns($row, 'F', 'G'),
            $this->getFrenchEnglishTextValueForColumns($row, 'H', 'I', true),
            $this->getFrenchEnglishTextValueForColumns($row, 'J', 'K'),
            $this->getTextValueForColumn($row, 'L'),
            $this->getTextValueForColumn($row, 'M'),
            $this->getFloatValueFromColumn($row, 'N')
        );
    }

    protected function getDataStartLine(): int
    {
        return 3;
    }
}
