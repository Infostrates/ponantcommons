<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Exceptions;

use Exception;

final class InvalidFile extends Exception
{
}
