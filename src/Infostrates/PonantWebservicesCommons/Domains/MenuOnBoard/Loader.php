<?php

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard;

use Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard\Parser as AbstractParser;
use InvalidArgumentException;

abstract class Loader
{
    protected const LANGUAGE_FR = 'fr';
    protected const LANGUAGE_EN = 'en';

    /** @var AbstractParser */
    private $parser;

    /**
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param string|string[] $fileNames
     * @param string $language
     * @param float  $vatPercentage VAT (in %) to apply on price without taxes
     * @return object[]
     * @throws Exceptions\InvalidFile
     */
    public function load($fileNames, string $language, float $vatPercentage = 0.0): array
    {
        $inputLineList = $this->parser->parseFile($fileNames);

        return $this->loadFromInputLineList($inputLineList, $language, $vatPercentage);
    }

    /**
     * @param mixed  $value
     * @param string $language
     * @return mixed
     */
    protected function loadValue($value, string $language)
    {
        if ($value instanceof FrenchEnglishTextValue) {
            if ($language === self::LANGUAGE_FR) {
                return $value->getFrenchText();
            }

            if ($language === self::LANGUAGE_EN) {
                return $value->getEnglishText();
            }

            throw new InvalidArgumentException('Unknown language : ' . $language);
        }

        return $value;
    }

    /**
     * @param mixed  $value
     * @param string $language
     * @param float  $vatPercentage
     * @return float
     */
    protected function loadValueAndApplyVat($value, string $language, float $vatPercentage): float
    {
        if ($value instanceof FrenchEnglishTextValue) {
            if ($language === self::LANGUAGE_FR) {
                $value = (float)$value->getFrenchText();
            } elseif ($language === self::LANGUAGE_EN) {
                $value = (float)$value->getEnglishText();
            } else {
                throw new InvalidArgumentException('Unknown language : ' . $language);
            }
        }

        if (is_float($value)) {
            $value = round($value * (1 + ($vatPercentage / 100)), 2);
        } else {
            throw new InvalidArgumentException('Got a non float value to apply vat on.');
        }

        return $value;
    }

    /**
     * @return LineParseError[]
     */
    public function getLastLineParseError(): array
    {
        return $this->parser->getLastLineParseError();
    }

    /**
     * @param object[] $inputLineList
     * @param string   $language
     * @param float    $vatPercentage VAT (in %) to apply on price without taxes
     * @return object[]
     */
    abstract protected function loadFromInputLineList(array $inputLineList, string $language, float $vatPercentage): array;
}
