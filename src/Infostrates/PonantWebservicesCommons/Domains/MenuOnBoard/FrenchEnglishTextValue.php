<?php

declare(strict_types=1);

namespace Infostrates\PonantWebservicesCommons\Domains\MenuOnBoard;

final class FrenchEnglishTextValue
{
    /** @var string */
    private $frenchText;

    /** @var string */
    private $englishText;

    /**
     * @param string $frenchText
     * @param string $englishText
     */
    public function __construct(string $frenchText, string $englishText)
    {
        $this->frenchText = $frenchText;
        $this->englishText = $englishText;
    }

    /**
     * @return string
     */
    public function getFrenchText(): string
    {
        return $this->frenchText;
    }

    /**
     * @return string
     */
    public function getEnglishText(): string
    {
        return $this->englishText;
    }

    public function isEmpty(): bool
    {
        return empty($this->frenchText) || empty($this->englishText);
    }
}
